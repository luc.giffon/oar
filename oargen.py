#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function
from __future__ import unicode_literals

import argparse
# import logging
import subprocess


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', nargs='?',
                        help="Command to use for the job (in passive mode)")
    parser.add_argument('argument', nargs=argparse.REMAINDER,
                        help="Arguments of the command (in passive mode)")
    parser.add_argument('-n', '--name',
                        help="Name to give to the job")
    parser.add_argument('-b', '--besteffort', action="store_true",
                        help="Launch job in besteffort mode")
    parser.add_argument('-t', '--time', default=12, type=int,
                        help="Estimated maximum duration of the job (in hours)")
    parser.add_argument('-g', '--gpu', action="store_true",
                        help="If True, reserves only cores with GPUs")
    parser.add_argument('-c', '--core', default=1, type=int,
                        help="Number of cores to reserve")
    parser.add_argument('-H', '--host',
                        help="Name of the host (SQL LIKE syntax accepted)")
    parser.add_argument('-i', '--interactive', action="store_true",
                        help="Launch job in interactive mode")
    parser.add_argument('-C', '--checkpoint', type=int, metavar="SECONDS",
                        help="Enable checkpoint signals with the given delay (in seconds)")
    parser.add_argument('-r', '--run', action="store_true",
                        help="Run the command")
    # parser.add_argument('-l', '--logger', default='INFO',
    #                     choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    #                     help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    # numeric_level = getattr(logging, args.logger.upper(), None)
    # if not isinstance(numeric_level, int):
    #     raise ValueError("Invalid log level: {}".format(args.logger))
    # logging.basicConfig(level=numeric_level)

    return args


def main():
    args = argparser()

    command = ["oarsub"]
    command.append("-p")
    properties = ""
    if args.gpu:
        properties += "(gpu IS NOT NULL)"
    else:
        properties += "(gpu IS NULL)"
    if args.host is not None:
        properties += " AND host LIKE '{}'".format(args.host)
    properties += ""
    command.append(properties)

    command.append("-l")
    ressources = "core={},walltime={}:00:00".format(args.core, args.time)
    command.append(ressources)

    if args.name is not None:
        command.append("-n")
        command.append(args.name)
        command.append("-O")
        command.append("{}.%jobid%.stdout".format(args.name))
        command.append("-E")
        command.append("{}.%jobid%.stderr".format(args.name))

    if args.besteffort:
        command.extend(["-t", "besteffort", "-t", "idempotent"])

    if args.checkpoint is not None:
        command.extend(["--checkpoint", args.checkpoint])

    if args.interactive:
        command.append('-I')
    else:
        job_command = [args.command] + args.argument
        command.append(" ".join(job_command))

    print(subprocess.list2cmdline(command))
    if args.run:
        subprocess.call(command)


if __name__ == '__main__':
    main()
