#! /bin/zsh

if [[ $# -lt 2 ]]; then
    echo "Usage: $0 environment_name command [arg1..argn]" >&2
    exit 1
fi

environment="$1"
shift 1

source activate $environment

if [[ $environment =~ ^keras.* || $environment =~ ^pytorch.* ]]; then
    export CUDA_HOME=/usr/local/cuda-8.0
    export CUDA_ROOT=$CUDA_HOME
	export PATH=$CUDA_HOME/bin:$PATH
	export MANPATH=$CUDA_HOME/doc/man:$MANPATH
	export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
fi

eval "$@"
